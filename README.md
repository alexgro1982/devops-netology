# devops-netology

line 2

Описание terraform/.gitignore

1. Распространяется на каталог terraform и подкаталоги.
2. Игнорируются все скрытые каталоги и подкаталоги с именем .terraform
3. файлы, имеющие расширение tfstate
4. файлы crash.log
5. файлы, имеющие расширение tfvars
6. файлы override.tf, override.tf.json, а также файлы, имена которых заканчиваются на _override.tf и _override.tf.json
7. скрытый файл .terraformrc и файл terraform.rc

